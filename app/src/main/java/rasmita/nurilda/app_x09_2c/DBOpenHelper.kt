package rasmita.nurilda.app_x09_2c

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context) : SQLiteOpenHelper (context, DB_Name, null, DB_Ver) {
    companion object{
        val DB_Name = "musik"
        val DB_Ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val tmp = "create table music_player(id_music text, id_cover text, music_title text)"
        val insmp = "insert into music_player values('0x7f0c0000', '0x7f060070', 'Omae Wa Mou'),('0x7f0c0001', '0x7f060057', 'Beautiful In White'),('0x7f0c0002', '0x7f060071', 'Kisah Klasik')"

        db?.execSQL(tmp)
        db?.execSQL(insmp)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }

}
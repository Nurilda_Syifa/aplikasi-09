package rasmita.nurilda.app_x09_2c

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.icu.util.TimeUnit
import android.media.AsyncPlayer
import android.media.MediaPlayer
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() ,View.OnClickListener, SeekBar.OnSeekBarChangeListener{


    val daftarLagu = intArrayOf(R.raw.musik1, R.raw.musik2, R.raw.musik3)
//    val daftarVidio = intArrayOf(R.raw.mv1, R.raw.mv2, R.raw.mv3)
    val daftarCover = intArrayOf(R.drawable.umaru, R.drawable.bt21, R.drawable.v)
    val daftarjudul = arrayOf("Omae Wa Mou", "Beautiful In White", "Kisah Klasik")

    var posLaguSkrg = 0
    var judul = ""

//    var posCoverSkrg = 0
    var posVidioSkrg = 0
//    var durasiLagu = 0
    var handler = Handler()
    lateinit var  db : SQLiteDatabase
    lateinit var adaplist : SimpleCursorAdapter

    lateinit var mediaPlayer: MediaPlayer
    lateinit var mediaController: MediaController

    var max : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        db= DBOpenHelper(this).writableDatabase
        mediaController = MediaController(this)
        mediaPlayer =  MediaPlayer()
        //seekbar
        seekSong.max= 100
        seekSong.progress = 0
        seekSong.setOnSeekBarChangeListener(this)
        //btn
        btnPlay.setOnClickListener(this)
        btnNext.setOnClickListener(this)
        btnPrev.setOnClickListener(this)
        btnStop.setOnClickListener(this)
//        mediaController.setPrevNextListeners(nextVid,prevVid)
//        mediaController.setAnchorView(videoView)
//        videoView.setMediaController(mediaController)
//        videoSet(posVidioSkrg)

        listMusik()

        //list view
        lsplaylist.setOnItemClickListener(clik)
    }

//    var nextVid = View.OnClickListener { v:View ->
//        if(posVidioSkrg<(daftarVidio.size-1)) posVidioSkrg++
//        else posVidioSkrg = 0
//        videoSet(posVidioSkrg)
//    }
//    var prevVid = View.OnClickListener { v:View ->
//        if(posVidioSkrg>0) posVidioSkrg--
//        else posVidioSkrg = daftarVidio.size-1
//        videoSet(posVidioSkrg)
//    }
//    fun videoSet(pos : Int){
//        videoView.setVideoURI(Uri.parse("android.resource://"+packageName+"/"+daftarVidio[pos]))
//
//
//    }



    //read data base
    fun getDBObject():SQLiteDatabase{

        return db
    }

    //tampil list view
    fun listMusik(){
        val cursor : Cursor = db.query("music_player", arrayOf("id_music as _id","id_cover","music_title"),null,null,null,null,"id_music asc")
        adaplist = SimpleCursorAdapter(this,R.layout.playlist,cursor,
            arrayOf("_id","id_cover","music_title"), intArrayOf(R.id.txIdMsk, R.id.txIdCvr, R.id.textView7),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        lsplaylist.adapter = adaplist
    }

    fun milliSecondToString(ms : Int):String{
        var detik = java.util.concurrent.TimeUnit.MILLISECONDS.toSeconds(ms.toLong())
        val menit = java.util.concurrent.TimeUnit.SECONDS.toMinutes(detik)
        detik = detik % 60
        return "$menit : $detik"

    }

    fun audioPlay(pos : Int){
        mediaPlayer = MediaPlayer.create(this, daftarLagu[pos])
        seekSong.max = mediaPlayer.duration
        txMaxText.setText(milliSecondToString(seekSong.max))

        txCrTime.setText(milliSecondToString(mediaPlayer.currentPosition))
        seekSong.progress = mediaPlayer.currentPosition
        imV.setImageResource(daftarCover[pos])
        txJudulLagu.setText(daftarjudul[pos])
        mediaPlayer.start()
        var updateSeekBarThread = UpdateSeekBarProgressThread()
        handler.postDelayed(updateSeekBarThread, 50)
    }

    fun audioNext(){
       if (mediaPlayer.isPlaying)mediaPlayer.stop()
        if (posLaguSkrg<(daftarLagu.size-1)){
            posLaguSkrg++
        } else{
            posLaguSkrg = 0
        }
        audioPlay(posLaguSkrg)
    }

    fun audioPrev(){
        if (mediaPlayer.isPlaying) mediaPlayer.stop()
        if (posLaguSkrg>0){
            posLaguSkrg--
        }else{
            posLaguSkrg = daftarLagu.size-1
        }
        audioPlay(posLaguSkrg)
    }

    fun audioStop(){
        if(mediaPlayer.isPlaying) mediaPlayer.stop()
    }

    inner class UpdateSeekBarProgressThread : Runnable{
        override fun run() {
            var currTime = mediaPlayer.currentPosition
            txCrTime.setText(milliSecondToString(currTime))
            seekSong.progress = currTime
            if (currTime != mediaPlayer.duration) handler.postDelayed(this,50)
        }

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnPlay ->{
                audioPlay(posLaguSkrg)
            }
            R.id.btnNext ->{
                audioNext()
            }
            R.id.btnPrev ->{
                audioPrev()
            }
            R.id.btnStop->{
                audioStop()
            }
        }
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {

    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {

    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        seekBar?.progress?.let { mediaPlayer.seekTo(it) }
    }
    val clik = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c : Cursor = parent.adapter.getItem(position) as Cursor
        judul = c.getString(c.getColumnIndex("music_title"))

        if (mediaPlayer.isPlaying)mediaPlayer.stop()
        audioPlay(c.position)

    }
}
